package com.bnepal.scholarshipglobal;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.bnepal.scholarshipglobal.home.HomeFragment;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.navigation_view)
    NavigationView navigationView;
    @BindView(R.id.drawer)
    DrawerLayout drawerLayout;
   /* @BindView(R.id.gad_bottom)
    LinearLayout adLinlay;*/


    public static AdView adView;
    LinearLayout adLinLay;
    String addCodeBannerAddKey="ca-app-pub-6991859984879123/6526133692";

    public static String FACEBOOK_URL = "https://www.facebook.com/allscholarshipnepal";
    public static String FACEBOOK_PAGE_ID = "allscholarshipnepal";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        navigationView.setNavigationItemSelectedListener(this);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank

                super.onDrawerOpened(drawerView);
            }
        };

        /**
         * Layout for the google ads
         */
        adLinLay = (LinearLayout) findViewById(R.id.gad_bottom);
        adView = new AdView(this);

        getAds(addCodeBannerAddKey);


        //Setting the actionbarToggle to drawer layout
        drawerLayout.addDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessay or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();


       /* try {
            final AdView adView = new AdView(getApplicationContext());
            adView.setAdUnitId("ca-app-pub-6991859984879123~4695689690");
            adView.setAdSize(com.google.android.gms.ads.AdSize.SMART_BANNER);

            adLinlay.addView(adView);

            final AdRequest.Builder adReq = new AdRequest.Builder().addTestDevice("26245F8D8C2C22AC3A45E72C5D7D9E18");
            final AdRequest adRequest = adReq.build();
            adView.loadAd(adRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        switchFragment(0);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (item.isChecked()) item.setChecked(false);
        else item.setChecked(true);
        switch (item.getItemId()) {
            case R.id.home:
                switchFragment(0);
                break;
            case R.id.rateUs:
                switchFragment(1);
                break;
            case R.id.likeUs:
                switchFragment(2);
                break;
            case R.id.settings:
                switchFragment(3);
                break;
        }
        return false;
    }

    private void switchFragment(int position) {
        switch (position) {

            case 0:
                HomeFragment homeFragment = new HomeFragment();
                Bundle basket = new Bundle();
                basket.putString("url", "api/category");
                homeFragment.setArguments(basket);
                android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame, homeFragment);
                fragmentTransaction.commit();

                drawerLayout.closeDrawers();
                break;
            case 1:


                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName())));
                }
                drawerLayout.closeDrawers();
                break;
            case 2:
                Timber.d("on case 2");
                Intent facebookIntent = new Intent(Intent.ACTION_VIEW);
                String facebookUrl = getFacebookPageURL(this);
                facebookIntent.setData(Uri.parse(facebookUrl));
                startActivity(facebookIntent);

                drawerLayout.closeDrawers();
                break;
            case 3:
                Timber.d("on case 3");

                drawerLayout.closeDrawers();
                break;


            default:
                break;

        }
    }

    public String getFacebookPageURL(Context context) {
        PackageManager packageManager = context.getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) { //newer versions of fb app
                return "fb://facewebmodal/f?href=" + FACEBOOK_URL;
            } else { //older versions of fb app
                return "fb://page/" + FACEBOOK_PAGE_ID;
            }
        } catch (PackageManager.NameNotFoundException e) {
            return FACEBOOK_URL; //normal web url
        }
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(navigationView)) {
            drawerLayout.closeDrawers();
        } else {
            super.onBackPressed();
        }
    }
    /**
     * Loads the google ads in the layout
     */
    private void getAds(String addCodeBannerAddKey) {


        adView.setAdUnitId(addCodeBannerAddKey);
        adView.setAdSize(com.google.android.gms.ads.AdSize.SMART_BANNER);
        adLinLay.addView(adView);

        final AdRequest.Builder adReq = new AdRequest.Builder();
        final AdRequest adRequest = adReq.build();
        adView.loadAd(adRequest);

    }

}
