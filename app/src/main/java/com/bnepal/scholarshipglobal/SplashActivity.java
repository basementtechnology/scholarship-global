package com.bnepal.scholarshipglobal;

/**
 * Created by VRLab on 1/4/2017.
 */

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends Activity {
    public String appVersion;
    InterstitialAd mInterstitialAd;
    Timer waitTimer;
    Boolean interstitialCanceled=false;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        getAppVersion();

        try {
            TextView textView =(TextView) findViewById(R.id.version);
            String str="Version"+ appVersion+"\nCopyright © 2016 BNepal\nAll Rights Reserved.";
            textView.setText(str);
        } catch (Exception ex){

        }

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-6991859984879123/7509555291");

        //"ca-app-pub-6991859984879123/8002866894"

        requestNewInterstitial();

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                if (mInterstitialAd.isLoaded()) {
                    waitTimer.cancel();
                    mInterstitialAd.show();
                }
            }

            @Override
            public void onAdClosed() {

                startMainActivity();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                startMainActivity();
            }
        });
        waitTimer = new Timer();
        waitTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                interstitialCanceled = true;
                SplashActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        startMainActivity();
                    }
                });
            }
        }, 5000);
    }

    private void startMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onPause() {
        waitTimer.cancel();
        interstitialCanceled = true;
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else if (interstitialCanceled) {
            startMainActivity();
        }
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("87A0CA5CB725CAE55A02134DFB17CABB")
                .build();

        mInterstitialAd.loadAd(adRequest);
    }

        /*Thread timerThread = new Thread(){
            public void run(){
                try{
                    sleep(3000);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }finally{
                    Intent intent = new Intent(SplashActivity.this,MainActivity.class);
                    startActivity(intent);
                }
            }
        };
        timerThread.start();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        finish();


    }*/
    /**
     * Access the package information and gets the current version of the application from build.gradle
     */
    public void getAppVersion() {

        PackageInfo pInfo = null;

        try {

            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            appVersion = pInfo.versionName;

        } catch (PackageManager.NameNotFoundException e) {

            e.printStackTrace();

        }

    }

}
