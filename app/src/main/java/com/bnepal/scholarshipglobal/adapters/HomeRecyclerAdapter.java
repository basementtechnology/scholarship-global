package com.bnepal.scholarshipglobal.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bnepal.scholarshipglobal.R;
import com.bnepal.scholarshipglobal.home.HomePresImpl;
import com.bnepal.scholarshipglobal.utils.Category;
import com.squareup.picasso.Picasso;


import java.util.List;


/**
 * Created by nitv on 18/03/16.
 */
public class HomeRecyclerAdapter extends RecyclerView.Adapter<HomeRecyclerAdapter.ViewHolder> {
    public static List<Category> categoryList;

    private Context context;

    HomePresImpl homePres;
    public HomeRecyclerAdapter(Context context, List<Category> categoryList, HomePresImpl homePres) { //Channel channel, Vod vod, String news
        this.categoryList = categoryList;
        this.context = context;
        this.homePres = homePres;
    }

    @Override
    public HomeRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_single_item, null);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.tvTitle.setText(categoryList.get(position).getName());
        holder.tvDesc.setText(categoryList.get(position).getDescription());
        Picasso.with(context).load(categoryList.get(position).getImage()).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        protected ImageView imageView;
        protected TextView tvTitle,tvDesc;

        public ViewHolder(View view) {
            super(view);
            itemView.setClickable(true);
            itemView.setOnClickListener(this);
            this.imageView = (ImageView) view.findViewById(R.id.imageView);
            this.tvTitle = (TextView) view.findViewById(R.id.title);
            this.tvDesc=(TextView)view.findViewById(R.id.desc);
        }

        @Override
        public void onClick(View v) {

            int position = getAdapterPosition();
            homePres.fragmentTransaction(categoryList.get(position).getId());

        }


    }


}
