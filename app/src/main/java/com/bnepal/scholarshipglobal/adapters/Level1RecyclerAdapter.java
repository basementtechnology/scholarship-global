package com.bnepal.scholarshipglobal.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bnepal.scholarshipglobal.R;
import com.bnepal.scholarshipglobal.home.HomePresImpl;
import com.bnepal.scholarshipglobal.home_lvl1.Level1PresImpl;
import com.bnepal.scholarshipglobal.utils.Product;
import com.squareup.picasso.Picasso;

import java.util.List;


/**
 * Created by nitv on 18/03/16.
 */
public class Level1RecyclerAdapter extends RecyclerView.Adapter<Level1RecyclerAdapter.ViewHolder> {
    public static List<Product> productList;

    private Context context;

    Level1PresImpl level1Pres;
    public Level1RecyclerAdapter(Context context, List<Product> productList, Level1PresImpl level1Pres) { //Channel channel, Vod vod, String news
        this.productList = productList;
        this.context = context;
        this.level1Pres = level1Pres;
    }

    @Override
    public Level1RecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_single_item, null);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.tvTitle.setText(productList.get(position).getName());
        holder.tvDesc.setText(productList.get(position).getDescription1());
        Picasso.with(context).load(productList.get(position).getImage()).into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        protected ImageView imageView;
        protected TextView tvTitle,tvDesc;

        public ViewHolder(View view) {
            super(view);
            itemView.setClickable(true);
            itemView.setOnClickListener(this);
            this.imageView = (ImageView) view.findViewById(R.id.imageView);
            this.tvTitle = (TextView) view.findViewById(R.id.title);
            this.tvDesc=(TextView)view.findViewById(R.id.desc);
        }

        @Override
        public void onClick(View v) {

            int position = getAdapterPosition();
            level1Pres.fragmentTransaction(productList.get(position).getId());

        }


    }


}
