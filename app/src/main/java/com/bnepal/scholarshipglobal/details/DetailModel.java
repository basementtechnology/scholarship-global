package com.bnepal.scholarshipglobal.details;

import com.bnepal.scholarshipglobal.utils.LinkConfig;
import com.bnepal.scholarshipglobal.utils.Product;
import com.bnepal.scholarshipglobal.utils.SetupRetrofit;

import retrofit2.Retrofit;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by nitv on 22/11/16.
 */

public class DetailModel implements DetailsApiInterface.DetailInteractor {
    DetailsApiInterface.DetailListener detailListener;
    SetupRetrofit setupRetrofit;
    public DetailModel(DetailsApiInterface.DetailListener detailListener) {
        this.detailListener = detailListener;
        setupRetrofit = new SetupRetrofit();
    }

    @Override
    public void sendDataToModel(String url) {
        Retrofit retrofit = setupRetrofit.getRetrofit(LinkConfig.BASE_URL);
        DetailsApiInterface detailsApiInterface =  retrofit.create(DetailsApiInterface.class);


        Observable<Product> observable = detailsApiInterface.getProduct(url);
        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).unsubscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Product>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();

                    }

                    @Override
                    public void onNext(Product product) {
                       detailListener.sendDataToPres(product);

                    }
                });
    }
}
