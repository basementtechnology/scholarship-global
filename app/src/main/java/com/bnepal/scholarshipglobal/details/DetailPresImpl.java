package com.bnepal.scholarshipglobal.details;

import com.bnepal.scholarshipglobal.home_lvl1.Level1ApiInterface;
import com.bnepal.scholarshipglobal.home_lvl1.Level1Model;
import com.bnepal.scholarshipglobal.utils.Product;

/**
 * Created by nitv on 22/11/16.
 */

public class DetailPresImpl implements DetailsApiInterface.DetailListener,DetailsApiInterface.DetailPresenter {
    DetailsApiInterface.DetailView detailView;
    DetailsApiInterface.DetailInteractor detailInteractor;
    public DetailPresImpl(DetailsApiInterface.DetailView detailView) {
        this.detailView = detailView;
        detailInteractor = new DetailModel(this);
    }


    @Override
    public void sendDataToPres(Product product) {
        detailView.setDataToList(product);
    }

    @Override
    public void sendDataToModel(String url) {
        detailInteractor.sendDataToModel(url);
    }
}
