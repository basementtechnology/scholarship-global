package com.bnepal.scholarshipglobal.details;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bnepal.scholarshipglobal.R;
import com.bnepal.scholarshipglobal.utils.Product;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nitv on 14/12/16.
 */

public class DetailsActivity extends Activity implements DetailsApiInterface.DetailView {
    @BindView(R.id.author)
    TextView tvName;
    @BindView(R.id.quote)
    TextView tvDesc;
    @BindView(R.id.moreInfo)
    TextView tvEligibility;
    @BindView(R.id.how_to_apply)
    TextView howToApply;
    @BindView(R.id.selection_process)
    TextView selectionProcess;
//    @BindView(R.id.tvDesc5)
//    TextView tvDesc5;
//    @BindView(R.id.tvDesc6)
//    TextView tvDesc6;
    @BindView(R.id.backdrop)
    ImageView ivBackDrop;
    DetailPresImpl detailPres;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_activity);
        ButterKnife.bind(this);
        int id = getIntent().getIntExtra("id", 0);
        String url = "api/product-detail/" + id;
        detailPres = new DetailPresImpl(this);
        detailPres.sendDataToModel(url);
    }

    @Override
    public void setDataToList(Product product) {
        Picasso.with(this).load(product.getImage()).into(ivBackDrop);
        tvName.setText(product.getName());
        tvDesc.setText(product.getDescription1());
        tvEligibility.setText(product.getEligibilityCriteria());
        howToApply.setText(product.getWhenAndHowToApply());
        selectionProcess.setText(product.getSelectionProcess());
//        tvDesc5.setText(product.getDescription5());
//        tvDesc6.setText(product.getDescription6());
    }
}
