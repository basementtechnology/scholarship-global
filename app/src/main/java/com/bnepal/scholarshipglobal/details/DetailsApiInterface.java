package com.bnepal.scholarshipglobal.details;

import com.bnepal.scholarshipglobal.utils.Category;
import com.bnepal.scholarshipglobal.utils.Product;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Url;
import rx.Observable;

/**
 * Created by anilpaudel on 11/21/16.
 */

public interface DetailsApiInterface {

//    @GET("api/category")
    @GET
    Observable<Product> getProduct(@Url String url);

    interface DetailView {
        void setDataToList(Product product);
    }
    interface DetailPresenter {
        void sendDataToModel(String url);
    }
    interface DetailInteractor {
        void sendDataToModel(String url);
    }
    interface DetailListener {
        void sendDataToPres(Product product);
    }
}
