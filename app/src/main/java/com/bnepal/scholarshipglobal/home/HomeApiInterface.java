package com.bnepal.scholarshipglobal.home;

import com.bnepal.scholarshipglobal.utils.Category;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;
import rx.Observable;

/**
 * Created by anilpaudel on 11/21/16.
 */

public interface HomeApiInterface {

//    @GET("api/category")
    @GET
    Observable<List<Category>> getCategories(@Url String url);

    interface HomeView{
        void setDataToList(List<Category> categoryList);
        void doFragmentTransaction(int id);
        void showProgress();
        void hideProgress();
    }
    interface HomePresenter{
        void sendDataToModel(String url);
        void fragmentTransaction(int id);
    }
    interface HomeInteractor{
        void sendDataToModel(String url);
    }
    interface HomeListener{
        void sendDataToPres(List<Category> categorieList);
    }
}
