package com.bnepal.scholarshipglobal.home;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.bnepal.scholarshipglobal.R;
import com.bnepal.scholarshipglobal.adapters.HomeRecyclerAdapter;
import com.bnepal.scholarshipglobal.home_lvl1.Level1Fragment;
import com.bnepal.scholarshipglobal.utils.Category;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by anilpaudel on 11/21/16.
 */

public class HomeFragment extends Fragment implements HomeApiInterface.HomeView {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.frame)
    FrameLayout frameLayout;
    HomePresImpl homePres;
    @BindView(R.id.activity_main_swipe_refresh_layout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.home_fragment, container, false);
        ButterKnife.bind(this, rootView);

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
        final String url = getArguments().getString("url");

        homePres = new HomePresImpl(this);
        homePres.sendDataToModel(url);

        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, R.color.colorAccent, R.color.colorPrimaryDark);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                homePres.sendDataToModel(url);
                mSwipeRefreshLayout.setRefreshing(true);
            }
        });

        return rootView;
    }

    @Override
    public void setDataToList(List<Category> categoryList) {
        recyclerView.setAdapter(new HomeRecyclerAdapter(getActivity(), categoryList, homePres));
    }

    @Override
    public void doFragmentTransaction(int id) {
        recyclerView.setVisibility(View.GONE);
        frameLayout.setVisibility(View.VISIBLE);
        Level1Fragment level1Fragment = new Level1Fragment();
        Bundle basket = new Bundle();
        basket.putInt("id", id);
        level1Fragment.setArguments(basket);
        android.support.v4.app.FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame, level1Fragment);
        fragmentTransaction.addToBackStack("");
        fragmentTransaction.commit();

    }

    @Override
    public void showProgress() {
        mSwipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        mSwipeRefreshLayout.setRefreshing(false);
    }
}
