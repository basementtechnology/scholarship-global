package com.bnepal.scholarshipglobal.home;

import com.bnepal.scholarshipglobal.utils.Category;
import com.bnepal.scholarshipglobal.utils.LinkConfig;
import com.bnepal.scholarshipglobal.utils.SetupRetrofit;

import java.util.List;

import retrofit2.Retrofit;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by nitv on 22/11/16.
 */

public class HomeModel implements HomeApiInterface.HomeInteractor {
    HomeApiInterface.HomeListener homeListener;
    SetupRetrofit setupRetrofit;
    public HomeModel(HomeApiInterface.HomeListener homeListener) {
        this.homeListener = homeListener;
        setupRetrofit = new SetupRetrofit();
    }

    @Override
    public void sendDataToModel(String url) {
        Retrofit retrofit = setupRetrofit.getRetrofit(LinkConfig.BASE_URL);
        HomeApiInterface homeApiInterface =  retrofit.create(HomeApiInterface.class);


        Observable<List<Category>> observable = homeApiInterface.getCategories(url);
        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).unsubscribeOn(Schedulers.io())
                .subscribe(new Subscriber<List<Category>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();

                    }

                    @Override
                    public void onNext(List<Category> categoryList) {
                       homeListener.sendDataToPres(categoryList);

                    }
                });
    }
}
