package com.bnepal.scholarshipglobal.home;

import com.bnepal.scholarshipglobal.utils.Category;

import java.util.List;

/**
 * Created by nitv on 22/11/16.
 */

public class HomePresImpl implements HomeApiInterface.HomePresenter,HomeApiInterface.HomeListener {
    HomeApiInterface.HomeView homeView;
    HomeApiInterface.HomeInteractor homeInteractor;
    public HomePresImpl(HomeApiInterface.HomeView homeView) {
        this.homeView = homeView;
        homeInteractor = new HomeModel(this);
    }

    @Override
    public void sendDataToPres(List<Category> categorieList) {
        homeView.setDataToList(categorieList);
        homeView.hideProgress();
    }

    @Override
    public void sendDataToModel(String url) {
        homeView.showProgress();
        homeInteractor.sendDataToModel(url);
    }

    @Override
    public void fragmentTransaction(int id) {
        homeView.doFragmentTransaction(id);
    }
}
