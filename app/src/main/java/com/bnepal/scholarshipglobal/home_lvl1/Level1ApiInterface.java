package com.bnepal.scholarshipglobal.home_lvl1;

import com.bnepal.scholarshipglobal.utils.Category;
import com.bnepal.scholarshipglobal.utils.Product;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Url;
import rx.Observable;

/**
 * Created by anilpaudel on 11/21/16.
 */

public interface Level1ApiInterface {

//    @GET("api/category")
    @GET
    Observable<List<Product>> getProducts(@Url String url);

    interface Level1View{
        void setDataToList(List<Product> productList);
        void doFragmentTransaction(int id);
        void showProgress();
        void hideProgress();
    }
    interface Level1Presenter{
        void sendDataToModel(String url);
        void fragmentTransaction(int id);
    }
    interface Level1Interactor{
        void sendDataToModel(String url);
    }
    interface Level1Listener{
        void sendDataToPres(List<Product> productList);
    }
}
