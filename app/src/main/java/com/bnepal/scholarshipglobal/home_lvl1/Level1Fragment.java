package com.bnepal.scholarshipglobal.home_lvl1;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bnepal.scholarshipglobal.details.DetailsActivity;
import com.bnepal.scholarshipglobal.R;
import com.bnepal.scholarshipglobal.adapters.Level1RecyclerAdapter;
import com.bnepal.scholarshipglobal.utils.Product;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nitv on 22/11/16.
 */

public class Level1Fragment extends Fragment implements Level1ApiInterface.Level1View{
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.activity_main_swipe_refresh_layout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    Level1PresImpl level1Pres;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.level_1_fragment,container,false);
        ButterKnife.bind(this,rootView);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        int id = getArguments().getInt("id",0);
       final String url = "api/product/"+id;
        level1Pres = new Level1PresImpl(this);
        level1Pres.sendDataToModel(url);

        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,R.color.colorAccent,R.color.colorPrimaryDark);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                level1Pres.sendDataToModel(url);
                mSwipeRefreshLayout.setRefreshing(true);
            }
        });

        return rootView;
    }

    @Override
    public void setDataToList(List<Product> productList) {
        recyclerView.setAdapter(new Level1RecyclerAdapter(getActivity(),productList,level1Pres));
    }

    @Override
    public void doFragmentTransaction(int id) {
        Intent intent = new Intent(getActivity(), DetailsActivity.class);
        intent.putExtra("id",id);
        getActivity().startActivity(intent);
    }

    @Override
    public void showProgress() {
        mSwipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        mSwipeRefreshLayout.setRefreshing(false);
    }


}
