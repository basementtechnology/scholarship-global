package com.bnepal.scholarshipglobal.home_lvl1;

import com.bnepal.scholarshipglobal.home.HomeApiInterface;
import com.bnepal.scholarshipglobal.utils.Category;
import com.bnepal.scholarshipglobal.utils.LinkConfig;
import com.bnepal.scholarshipglobal.utils.Product;
import com.bnepal.scholarshipglobal.utils.SetupRetrofit;

import java.util.List;

import retrofit2.Retrofit;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by nitv on 22/11/16.
 */

public class Level1Model implements Level1ApiInterface.Level1Interactor {
    Level1ApiInterface.Level1Listener level1Listener;
    SetupRetrofit setupRetrofit;
    public Level1Model(Level1ApiInterface.Level1Listener level1Listener) {
        this.level1Listener = level1Listener;
        setupRetrofit = new SetupRetrofit();
    }

    @Override
    public void sendDataToModel(String url) {
        Retrofit retrofit = setupRetrofit.getRetrofit(LinkConfig.BASE_URL);
        Level1ApiInterface level1ApiInterface =  retrofit.create(Level1ApiInterface.class);


        Observable<List<Product>> observable = level1ApiInterface.getProducts(url);
        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).unsubscribeOn(Schedulers.io())
                .subscribe(new Subscriber<List<Product>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();

                    }

                    @Override
                    public void onNext(List<Product> productList) {
                       level1Listener.sendDataToPres(productList);

                    }
                });
    }
}
