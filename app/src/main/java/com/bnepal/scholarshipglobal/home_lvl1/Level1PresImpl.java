package com.bnepal.scholarshipglobal.home_lvl1;

import com.bnepal.scholarshipglobal.home.HomeApiInterface;
import com.bnepal.scholarshipglobal.home.HomeModel;
import com.bnepal.scholarshipglobal.utils.Product;

import java.util.List;

/**
 * Created by nitv on 22/11/16.
 */

public class Level1PresImpl implements Level1ApiInterface.Level1Presenter,Level1ApiInterface.Level1Listener {
    Level1ApiInterface.Level1View level1View;
    Level1ApiInterface.Level1Interactor level1Interactor;
    public Level1PresImpl(Level1ApiInterface.Level1View level1View) {
        this.level1View = level1View;
        level1Interactor = new Level1Model(this);
    }

    @Override
    public void sendDataToPres(List<Product> productList) {
        level1View.setDataToList(productList);
        level1View.hideProgress();
    }

    @Override
    public void sendDataToModel(String url) {
        level1View.showProgress();
        level1Interactor.sendDataToModel(url);
    }

    @Override
    public void fragmentTransaction(int id) {
        level1View.doFragmentTransaction(id);
    }
}
