
package com.bnepal.scholarshipglobal.utils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Product {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description_1")
    @Expose
    private String description1;
    @SerializedName("eligibility_criteria")
    @Expose
    private String eligibilityCriteria;
    @SerializedName("when_and_how_to_apply")
    @Expose
    private String whenAndHowToApply;
    @SerializedName("selection_process")
    @Expose
    private String selectionProcess;
    @SerializedName("description_5")
    @Expose
    private String description5;
    @SerializedName("description_6")
    @Expose
    private String description6;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("image")
    @Expose
    private String image;


    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The description1
     */
    public String getDescription1() {
        return description1;
    }

    /**
     * 
     * @param description1
     *     The description_1
     */
    public void setDescription1(String description1) {
        this.description1 = description1;
    }

    public String getEligibilityCriteria() {
        return eligibilityCriteria;
    }

    public void setEligibilityCriteria(String eligibilityCriteria) {
        this.eligibilityCriteria = eligibilityCriteria;
    }

    public String getWhenAndHowToApply() {
        return whenAndHowToApply;
    }

    public void setWhenAndHowToApply(String whenAndHowToApply) {
        this.whenAndHowToApply = whenAndHowToApply;
    }

    public String getSelectionProcess() {
        return selectionProcess;
    }

    public void setSelectionProcess(String selectionProcess) {
        this.selectionProcess = selectionProcess;
    }

    /**
     * 
     * @return
     *     The description5
     */
    public String getDescription5() {
        return description5;
    }

    /**
     * 
     * @param description5
     *     The description_5
     */
    public void setDescription5(String description5) {
        this.description5 = description5;
    }

    /**
     * 
     * @return
     *     The description6
     */
    public String getDescription6() {
        return description6;
    }

    /**
     * 
     * @param description6
     *     The description_6
     */
    public void setDescription6(String description6) {
        this.description6 = description6;
    }

    /**
     * 
     * @return
     *     The categoryId
     */
    public String getCategoryId() {
        return categoryId;
    }

    /**
     * 
     * @param categoryId
     *     The category_id
     */
    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * 
     * @return
     *     The image
     */
    public String getImage() {
        return image;
    }

    /**
     * 
     * @param image
     *     The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    

}
